# ローカルの Kubernetes で開発する

## Run on Docker

- 2章から、このサンプルのコードをデプロイして試していく
- [GitLab.com](https://gitlab.com)へサインイン
- このサンプルのプロジェクトをForkする

![](image/fork-gitlab.png)

- 先程 Fork したコードを取得する
- 例： `$ git clone https://gitlab.com/koda3t/devops-test.git` ← koda3が私のアカウント名

```
$ git clone https://gitlab.com/{your_name}/devops-test.git
```

- Docker単体で動作確認

```
$ cd devops-test # クローンしたディレクトリに移動
$ docker build -t gcr.io/project_name/devops-test web/.
$ docker run -it --rm --name web -p 80:80 gcr.io/project_name/devops-test
```

- ブラウザで http://localhost にアクセスできることを確認


## Deploy to Kubernetes

```
$ kubectl apply -f k8s/deployment.yaml
$ kubectl apply -f k8s/service.yaml
$ kubectl get pod
$ kubectl get deployment
$ kubectl get service
```

- 同じく、ブラウザで http://localhost にアクセスできることを確認



-----

## スケールを試してみる

- `k8s/deployment.yaml` の `replicas: 1` の記述を `replicas: 10` に変更
  - スペックが厳しい場合は、 `3` くらいでもよいかも

```
$ kubectl get pod
$ kubectl apply -f k8s/deployment.yaml
$ kubectl get pod
```

- Podが3つになっている
- ブラウザでアクセスしてリロードを何回も行うと、接続しているPodが変わることが確認できる
  - 負荷分散している


## ローリングアップデート


- `web/public/index.html` の69行目の `message` を `Hello, DevOps World!!!` から `Test Change` に変更
- 新しいイメージをビルド

```
$ docker build -t gcr.io/project_name/devops-test:v2 web/.
```

- `k8s/deployment.yaml` の `image: gcr.io/project_name/devops-test` の記述を `image: gcr.io/project_name/devops-test:v2` に変更

```
$ kubectl apply -f k8s/deployment.yaml
$ kubectl get pod
```

- ブラウザでアクセスすると、古いバージョンから新しいバージョンに切り替わっていく
- 古いイメージのPodが消えて新しいコンテナが起動する
  - タイミングによっては、アクセスする際に古いバージョンにアクセスしたり新しいものにアクセスしたりする事が確認できる
  - 最近のマシンだと一瞬で切り替わるのでローカルでは確認しづらいかも
  - クラウドで 100つのレプリカとかを起動していると、順に切り替わっていくのが確認できる


- クリーンアップ

```
$ git checkout k8s/deployment.yaml
$ git checkout web/public/index.html
$ kubectl delete service web
$ kubectl delete deployment web-production
$ kubectl get pod          # No resources found. になる
$ kubectl get deployment   # No resources found. になる
$ kubectl get service
```



-----


## Skaffoldを使って開発する

### Skaffold のインストール

- https://skaffold.dev/docs/install/
- example: Install Skaffold to MacOS

```
$ brew install skaffold
```

- LinuxやWindowsの人は、上記のページから


### Skaffold を使ってローカル開発を開始

- Skaffoldが自動でDockerイメージをビルドする事を確認したいので、先程作成したDockerイメージを削除

```
$ docker rmi gcr.io/project_name/devops-test
$ docker rmi gcr.io/project_name/devops-test:v2
```

- Skaffoldを起動

```
$ skaffold dev
```

- `web/public/index.html` を編集してみる
- Skaffold が変更を検知し、コンテナイメージのビルド、k8sへのデプロイが自動で実行されることが確認できる
  - K8sへのデプロイは少し時間がかかるので、更新できていないときは `kubectl get pod` でPodの状態を確認してみる
- 確認したら終了


### Dockerイメージのクリーンアップ

- Skaffoldは変更するたびに新しいDockerImageを作成するので、以下のようなコマンドで一括削除できる
  - Windowsでは動かないので一つづつ削除してください

```
$ kubectl get pod          # No resources found. になるまで待つ
$ docker rmi -f `docker images gcr.io/project_name/devops-test -q`
```


------


### (オプション) Skaffold のサンプルを実行を試す(Skaffold 公式に書いてある手順)
- Skaffold のサンプルを実行してみる(任意のディレクトリで実行してみる)

```
$ git clone https://github.com/GoogleContainerTools/skaffold
$ cd examples/getting-started
$ skaffold dev
```

- `main.go` を変更すると、コンテナイメージのビルド、k8sへのデプロイが自動で実行されることが確認できる



**おつかれさまでした。次のセクションに進みます。**
